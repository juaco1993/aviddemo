package com.globallogic.aviddemo.controller;

import com.globallogic.aviddemo.entity.User;
import com.globallogic.aviddemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    // Gets a user by ID
    @GetMapping("/{id}")
    public Optional<User> getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }


    // Create a new user
    @PutMapping
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }


}
