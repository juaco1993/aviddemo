package com.globallogic.aviddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AviddemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AviddemoApplication.class, args);
	}

}
