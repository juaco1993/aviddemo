package com.globallogic.aviddemo.service;

import com.globallogic.aviddemo.entity.User;
import com.globallogic.aviddemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    // Get user by ID
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }


    // Create a new user
    public User createUser(User user) {
        return userRepository.save(user);
    }
}
